// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <glew/glew.h>
#include <glew/wglew.h>
#include <GL\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "src/CGTexturedQuad.h"
#include "src/aiWrapper.h"
#include "src/CGPrincipleAxes.h"
#include "src/texture_loader.h"
#include "src/sceneVBOs.h"


using namespace std;
using namespace CoreStructures;

#pragma region Scene variables and resources

// Variables needed to track where the mouse pointer is so we can determine which direction it's moving in
int		mouse_x, mouse_y;
bool	mDown = false;
bool	wKeyDown, sKeyDown, aKeyDown, dKeyDown, qKeyDown, eKeyDown = false;
GUClock* mainClock = nullptr;

//
// Main scene resources
//
GUPivotCamera* mainCamera = nullptr;
CGPrincipleAxes* principleAxes = nullptr;
CGTexturedQuad* texturedQuad = nullptr;
const aiScene* aiTank;
const aiScene* aiTree;
const aiScene* aiLvl;
const aiScene* aiHut;
GLuint	tankTexture;
GLuint	tankTexture2;
GLuint	treeTexture;
GLuint	lvlTexture;
GLuint	hutTexture;

sceneVBOs* Tank;
sceneVBOs* Tree;
sceneVBOs* Lvl;
sceneVBOs* Hut;
GLuint meshShader;

float playerX = 5.0;
float playerY = 0.0;
float playerZ = 5.0;

float playerRX = 0.0;
float playerRY = 0.0;
float playerRZ = 0.0;

#pragma endregion


#pragma region Function Prototypes

void init(int argc, char* argv[]); // Main scene initialisation function
void update(void); // Main scene update function
void display(void); // Main scene render function

// Event handling functions
void mouseButtonDown(int button_id, int state, int x, int y);
void mouseMove(int x, int y);
void mouseWheel(int wheel, int direction, int x, int y);
void keyDown(unsigned char key, int x, int y);
void closeWindow(void);
void reportContextVersion(void);
void reportExtensions(void);

#pragma endregion


int main(int argc, char* argv[])
{
	init(argc, argv);
	glutMainLoop();

	// Stop clock and report final timing data
	if (mainClock) {

		mainClock->stop();
		mainClock->reportTimingData();
		mainClock->release();
	}

	return 0;
}


void init(int argc, char* argv[]) {

	// Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE/* | GLUT_MULTISAMPLE*/);
	glutSetOption(GLUT_MULTISAMPLE, 4);

	// Setup window
	int windowWidth = 800;
	int windowHeight = 600;
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("3D Example 01");
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	// Register callback functions
	glutIdleFunc(update); // Main scene update function
	glutDisplayFunc(display); // Main render function
	glutKeyboardFunc(keyDown); // Key down handler
	glutMouseFunc(mouseButtonDown); // Mouse button handler
	glutMotionFunc(mouseMove); // Mouse move handler
	glutMouseWheelFunc(mouseWheel); // Mouse wheel event handler
	glutCloseFunc(closeWindow); // Main resource cleanup handler


	// Initialise glew
	glewInit();

	// Initialise OpenGL...

	wglSwapIntervalEXT(0);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CCW); // Default anyway

	// Setup colour to clear the window
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Setup main camera
	float viewportAspect = (float)windowWidth / (float)windowHeight;
	mainCamera = new GUPivotCamera(0.0f, 10.0f, 15.0f, 65.0f, viewportAspect, 0.1f);

	principleAxes = new CGPrincipleAxes();

	texturedQuad = new CGTexturedQuad("textures\\glass.jpg");

	aiTank = aiImportModel(string("models\\Tank.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Tank = new sceneVBOs(aiTank);

	tankTexture = fiLoadTexture("textures\\tankcamo2.png", TextureProperties(false));
	tankTexture2 = fiLoadTexture("textures\\tankTex.jpg", TextureProperties(false));
	//Level code
	aiLvl = aiImportModel(string("models\\Lvl.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);



	Lvl = new sceneVBOs(aiLvl);

	lvlTexture = fiLoadTexture("textures\\sandstone.png", TextureProperties(false));

	
	aiTree = aiImportModel(string("models\\Tree.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Tree = new sceneVBOs(aiTank);

	treeTexture = fiLoadTexture("textures\\treetex.png", TextureProperties(false));
	
	aiHut = aiImportModel(string("models\\hut2.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);



	Hut = new sceneVBOs(aiLvl);

	hutTexture = fiLoadTexture("textures\\brick.png", TextureProperties(false));

	meshShader = setupShaders(string("shaders\\mesh_shader.vs"), string("shaders\\mesh_shader.fs"));

	// Setup and start the main clock
	mainClock = new GUClock();
}

// Main scene update function (called by FreeGLUT's main event loop every frame) 
void update(void) {

	// Update clock
	mainClock->tick();

	// Redraw the screen
	display();

	// Update the window title to show current frames-per-second and seconds-per-frame data
	char timingString[256];
	sprintf_s(timingString, 256, "Model Rendering CIS5013. Average fps: %.0f; Average spf: %f", mainClock->averageFPS(), mainClock->averageSPF() / 1000.0f);
	glutSetWindowTitle(timingString);

	if (wKeyDown == true) {
		playerZ = 1.0f + playerZ;
		wKeyDown = false;
	}
	if (sKeyDown == true) {
		playerZ = -1.0f + playerZ;
		sKeyDown = false;
	}
	if (aKeyDown == true) {
		playerX = 1.1f + playerX;
		aKeyDown = false;
	}
	if (dKeyDown == true) {
		playerX = -1.1f + playerX;
		dKeyDown = false;
	}
	if (qKeyDown == true) {
		playerRY = 0.05f + playerRY;
		qKeyDown = false;
	}
	if (eKeyDown == true) {
		playerRY = -0.05f + playerRY;
		eKeyDown = false;
	}
}


void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set viewport to the client area of the current window
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	// Get view-projection transform as a GUMatrix4
	GUMatrix4 T = mainCamera->projectionTransform() * mainCamera->viewTransform() * GUMatrix4::translationMatrix(-playerX,-playerY,-playerZ) * GUMatrix4::rotationMatrix(-playerRX,-playerRY,-playerRZ);

	if (principleAxes)
		principleAxes->render(T);

	
	glBindTexture(GL_TEXTURE_2D, tankTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 tankMatrix = GUMatrix4::translationMatrix(playerX, playerY, playerZ) * GUMatrix4::scaleMatrix(0.75, 0.75, 0.75) * GUMatrix4::rotationMatrix(playerRX, playerRY, playerRZ);
	GUMatrix4 TTank = T * tankMatrix;
	static GLuint mvpLocation = glGetUniformLocation(meshShader, "mvpMatrix");
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(TTank.M));
	Tank->render();

	glBindTexture(GL_TEXTURE_2D, tankTexture2);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 tankMatrix2 = GUMatrix4::translationMatrix(250.0,0.0,0.0) * GUMatrix4::scaleMatrix(0.75, 0.75, 0.75) * GUMatrix4::rotationMatrix(0.0,-1.6,0.0);
	GUMatrix4 TTank2 = T * tankMatrix2;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(TTank2.M));
	Tank->render();

	

	glBindTexture(GL_TEXTURE_2D, tankTexture2);
	glEnable(GL_TEXTURE_2D);
	tankMatrix2 = GUMatrix4::translationMatrix(250.0, 0.0, -250.0) * GUMatrix4::scaleMatrix(0.75, 0.75, 0.75) * GUMatrix4::rotationMatrix(0.0, -1.2, 0.0);
	GUMatrix4 TTank3 = T * tankMatrix2;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(TTank3.M));
	Tank->render();

	glBindTexture(GL_TEXTURE_2D, lvlTexture);
	glEnable(GL_TEXTURE_2D);
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));
	Lvl->render();
	
	glBindTexture(GL_TEXTURE_2D, treeTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 treeMatrix2 = GUMatrix4::translationMatrix(255.0, 0.0, 255.0) * GUMatrix4::scaleMatrix(1.0, 1.0, 1.0) * GUMatrix4::rotationMatrix(0.0, -1.6, 0.0);
	GUMatrix4 Tree2 = T * treeMatrix2;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(Tree2.M));
	Tree->render();

	
	glBindTexture(GL_TEXTURE_2D, hutTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 hutMatrix = GUMatrix4::translationMatrix(-250.0, 0.0, -250.0) * GUMatrix4::scaleMatrix(0.75,0.75,0.75) * GUMatrix4::rotationMatrix(0.0,-0.5,0.0);
	GUMatrix4 T2 = T * hutMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T2.M));
	Hut->render();
	
	hutMatrix = GUMatrix4::translationMatrix(-250.0, 0.0, 250.0) * GUMatrix4::scaleMatrix(0.75, 0.75, 0.75) * GUMatrix4::rotationMatrix(0.0, 2.5, 0.0);
	T2 = T * hutMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T2.M));
	Hut->render();

	//transparent objects
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	GUMatrix4 glassMatrix = GUMatrix4::translationMatrix(250.0, 0.0, 250.0) * GUMatrix4::scaleMatrix(100.0,100.0,100.0) * GUMatrix4::rotationMatrix(0.0,-2.5,0.0);
	GUMatrix4 T3 = T * glassMatrix;
	texturedQuad->render(T3);
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	glutSwapBuffers();
}



#pragma region Event handling functions

void mouseButtonDown(int button_id, int state, int x, int y) {

	if (button_id == GLUT_LEFT_BUTTON) {

		if (state == GLUT_DOWN) {

			mouse_x = x;
			mouse_y = y;

			mDown = true;

		}
		else if (state == GLUT_UP) {

			mDown = false;
		}
	}
}


void mouseMove(int x, int y) {

	int dx = x - mouse_x;
	int dy = y - mouse_y;

	if (mainCamera)
		mainCamera->transformCamera((float)-dy, (float)-dx, 0.0f);

	mouse_x = x;
	mouse_y = y;
}


void mouseWheel(int wheel, int direction, int x, int y) {

	if (mainCamera) {

		if (direction < 0)
			mainCamera->scaleCameraRadius(1.1f);
		else if (direction > 0)
			mainCamera->scaleCameraRadius(0.9f);
	}
}


void keyDown(unsigned char key, int x, int y) {

	// Toggle fullscreen (This does not adjust the display mode however!)
	if (key == 'f')
		glutFullScreenToggle();
	if (key == 'w')
		wKeyDown = true;
	if (key == 's')
		sKeyDown = true;
	if (key == 'a')
		aKeyDown = true;
	if (key == 'd')
		dKeyDown = true;
	if (key == 'q')
		qKeyDown = true;
	if (key == 'e')
		eKeyDown = true;
}


void closeWindow(void) {

	// Clean-up scene resources

	if (mainCamera)
		mainCamera->release();

	if (principleAxes)
		principleAxes->release();

	if (texturedQuad)
		texturedQuad->release();

	//if (exampleModel)
		//exampleModel->release();
}


#pragma region Helper Functions

void reportContextVersion(void) {

	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";
}

void reportExtensions(void) {

	cout << "Extensions supported...\n\n";

	const char* glExtensionString = (const char*)glGetString(GL_EXTENSIONS);

	char* strEnd = (char*)glExtensionString + strlen(glExtensionString);
	char* sptr = (char*)glExtensionString;

	while (sptr < strEnd) {

		int slen = (int)strcspn(sptr, " ");
		printf("%.*s\n", slen, sptr);
		sptr += slen + 1;
	}
}

#pragma endregion


#pragma endregion

