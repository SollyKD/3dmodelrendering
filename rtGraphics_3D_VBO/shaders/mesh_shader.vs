#version 460

uniform mat4 mvpMatrix;

layout (location=0) in vec3 vertexPos;
layout (location=1) in vec3 vertexTexCoord;
layout (location=6) in vec3 vertexNormal;

out vec3 texCoord;
out vec3 brightness;

vec3 calcLightContributionForPoint(vec3 vertexPos, vec3 vertexNormal,vec3 L, vec3 icolour, vec3 LK){
	
	vec3 D = L - vertexPos;
	float lengthD = length(D);
	vec3 D_ = normalize(D);
	float a = 1.0/(LK.x + LK.y*lengthD + LK.z*lengthD*lengthD);

	float lambertian = clamp(dot(D_, vertexNormal), 0.0, 1.0);

	return lambertian * a * icolour;
}

void main(void) {
	
	vec3 L[3];
	L[0] = vec3(0.0, 1000.0, 0.0);
	L[1] = vec3(250.0, 10.0, 250.0);
	L[2] = vec3(0.0, 0.0, 0.0);

	vec3 Lcol[3];
	Lcol[0] = vec3(1.0, 0.9, 1.0);
	Lcol[1] = vec3(0.0, 0.9, 0.0);
	Lcol[2] = vec3(1.0, 0.5, 0.0);

	vec3 LK1[3];
	LK1[0] = vec3(1.0, 0.001, 0.0);
	LK1[1] = vec3(1.0, 0.01, 0.0);
	LK1[2] = vec3(1.0, 0.01, 0.0);

	brightness = vec3(0.0,0.0,0.0);
	for(int i=0; i<3; i++){
		brightness  += calcLightContributionForPoint(vertexPos, vertexNormal, L[i], Lcol[i], LK1[i]);
	}
	


	texCoord = vertexTexCoord;
	gl_Position = mvpMatrix * vec4(vertexPos,1.0);
}
