#include "sceneVBOs.h"



sceneVBOs::sceneVBOs(const aiScene* scene)
{

	numModels = scene->mNumMeshes;

	modelArray = (meshVBOs**)malloc(numModels * sizeof(meshVBOs*));

	for (int i = 0; i < numModels; i++) {

		modelArray[i] = new meshVBOs(scene, i);

	}

}


void sceneVBOs::render()
{

	for (int i = 0; i < numModels; i++) {

		modelArray[i]->render();

	}

}
