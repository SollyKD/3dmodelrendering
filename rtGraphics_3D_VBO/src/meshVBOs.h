#pragma once
#include <glew/glew.h>
#include <CoreStructures/GUObject.h>
#include <CoreStructures/GUMatrix4.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
//#include "sceneVBOs.h"
#include "shader_setup.h"

class meshVBOs: public CoreStructures::GUObject
{
private:

	GLuint meshVAO;
	GLuint meshPosBuffer;
	GLuint meshTexCoordBuffer;
	GLuint meshNormalBuffer;
	GLuint meshFaceVBO;


	//GLuint lightVAO;
	//GLuint lightPosBuffer;
	//GLuint lightTexCoordBuffer;
	//GLuint lightFaceVBO;


	GLuint numfaces;

public:
	
	meshVBOs(const aiScene* scene, int meshIndex);

	void render();

};

