 #pragma once
#include <glew/glew.h>
#include <CoreStructures/GUObject.h>
#include <CoreStructures/GUMatrix4.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "meshVBOs.h"
#include "shader_setup.h"

class sceneVBOs : public CoreStructures::GUObject {

	GLuint	numModels;
	meshVBOs** modelArray;

public:

	sceneVBOs(const aiScene* scene);

	void render();

};

